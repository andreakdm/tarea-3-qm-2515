from chemlib import Element
import math
def calcular_rendimiento(rendimiento_teorico, rendimiento_real):
    # Calculo del rendimiento porcentual
    rendimiento_porcentual = (rendimiento_real / rendimiento_teorico) * 100

    # Devolver el resultado
    return rendimiento_porcentual
