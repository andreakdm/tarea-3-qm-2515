from chemlib import Element
import math
def ConcentracionH(h: int):
    '''
    cuando le das help, te dice que hace la funcion
    '''
    #Formula de pH = -log(H+)
    a = -1 * math.log(h,(10))
    #Retornamos el valor
    return a
