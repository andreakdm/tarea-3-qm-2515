from chemlib import Element
import math
def calcular_concentracion(moles, volumen_litros):
    try:
        # Calcula la concentración
        concentracion = moles / volumen_litros
        return concentracion
    except ZeroDivisionError:
        return "El volumen no puede ser cero."
    except Exception as e:
        return "Ha ocurrido un error: " + str(e)
