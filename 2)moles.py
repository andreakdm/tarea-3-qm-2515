from chemlib import Element
import math
def InterpretadorCompuesto(n):
    '''
    Funcion que divide en un array los elementos de un compuesto
    Input: formula de compuesto quimico, por ejemplo C6H6
    Output: Array con cada elemento y su respectiva cantidad de atomos, por ejemplo ['C6','H6']
    '''
    #largo de la cadena de chars
    largo = len(n)
    #array donde se guardarán los elementos
    elementos = []
    #Declaramos el ciclo
    i = 0
    while i < largo: #itera por todos los elementos de la cadena de caracteres
        
        #si el primer elemento de la cadena es mayuscula
        if n[i].isupper():
            inicio = i
            i += 1
            while i < largo and (n[i].islower() or n[i].isdigit()): #si el siguiente es minuscula o digito, ciclo hasta que consiga una mayuscula
                i += 1
            #agregamos el elemento al array con su numero de atomos
            elementos.append(n[inicio:i])
        else:
            pass
    return elementos
    def CalculoMoles(compuesto, masa):
    '''
    Funcion que calcula los moles mediante la formula masa/peso molecular
    '''
    #dividimos en un array el compuesto
    arrayElementos = InterpretadorCompuesto(compuesto)
    #vemos la cantidad de elementos
    largo = len(arrayElementos)
    #inicializamos la suma de pesoMolecular
    pesoMolecular = 0
    #ciclo para cada uno de los elementos
    i = 0
    while i < largo:
        #usamos el elemento y su numero
        elementoN = arrayElementos[i]
        #default cantidad de atomos (si no hay numero al final de la cadena)
        atomos = 1
        #vemos la cantidad de chars de un elemento
        largoElemento = len(elementoN)
        # Si el ultimo char es un digito, entonces hay un numero de atomos especificado
        if elementoN[largoElemento - 1].isdigit():
            # Extrae el numero de atomos del final de la cadena
            atomos = int(''.join(filter(str.isdigit, elementoN)))
            # Extrae el simbolo del elemento del inicio de la cadena
            elemento = Element(''.join(filter(str.isalpha, elementoN)))
        else:
            #si no hay digitos al final
            elemento = Element(elementoN)
        #uso de libreria ChemLib
        propiedades = elemento.properties
        pesoAtomico = propiedades['AtomicMass']
        #sumamos al peso molecular la cantidad de atomos + el peso atomico
        pesoMolecular += atomos * pesoAtomico
        i += 1
    #retornamos la formula
    return masa/pesoMolecular

        # peso molecular = suma de peso de todos los atomos
