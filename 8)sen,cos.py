import math

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)

def taylor_seno(x, n):
    seno = 0
    for i in range(n):
        signo = (-1)**i
        y = x * (3.14159265 / 180)  # Convertir a radianes
        seno = seno + ((y**(2.0*i+1))/factorial(2*i+1)) * signo
    return seno

# Prueba de la función
angle = 45  # ángulo en grados
terms = 80  # número de términos en la serie de Taylor

senoTay = taylor_seno(angle, terms)
senoMath = math.sin(math.radians(angle))

print("El seno de", angle, "sin modulo math es: ", senoTay)
print("El seno de", angle, "del modulo math es: ", senoMath)
if senoTay == senoMath:
    print("Para el angulo ", angle, "el seno es igual con ambos métodos")
else:
    print("El seno varía por ", senoMath - senoTay)


def taylor_coseno(x, n):
    coseno = 0
    for i in range(n):
        signo = (-1)**i
        y = x * (3.14159265 / 180)  # Convertir a radianes
        coseno = coseno + ((y**(2.0*i))/factorial(2*i)) * signo
    return coseno

# Prueba de la función
angulo = 45  # ángulo en grados
terminos = 80  # número de términos en la serie de Taylor

cosenoTay = taylor_coseno(angulo, terminos)
cosenoMath = math.cos(math.radians(angulo))

print("El coseno de", angulo, "sin modulo math es: ", cosenoTay)
print("El coseno de", angulo, "del modulo math es: ", cosenoMath)
if cosenoTay == cosenoMath:
    print("Para el angulo ", angulo, "el coseno es igual con ambos métodos")
else:
    print("El coseno varía por ", cosenoMath - cosenoTay)
