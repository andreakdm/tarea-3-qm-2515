from chemlib import Element
import math
def es_soluble(Kps, Q):
    if Q < Kps:
        return 'La solución es soluble'
    elif Q == Kps:
        return 'La solución está en equilibrio'
    else:
        return 'La solución es insoluble'
