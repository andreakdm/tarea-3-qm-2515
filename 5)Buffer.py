from chemlib import Element
import math
def calcular_pH(pKa, concentracion_base, concentracion_acido):

  # Calcular el logaritmo de la relación entre la base y el ácido
  log_relacion = math.log10(concentracion_base / concentracion_acido)
  # Calcular el pH usando la ecuación de Henderson-Hasselbalch
  pH = pKa + log_relacion
  return pH
